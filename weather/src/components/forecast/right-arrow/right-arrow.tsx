import React from "react";
import arrowRight from "../../../assets/img/icons/arrow-right.svg";
import '../forecast.scss';

type RightArrowTypeProps = {
  scrollToTheNextForecast?: () => void,
}

const RightArrowBlock: React.FC<RightArrowTypeProps> = ({
                                                          scrollToTheNextForecast
                                                      }) => {
   return (
      <div onClick={scrollToTheNextForecast}
           className="weather-blocks__arrow-right">
         <img src={arrowRight} alt="arrow-right"/>
      </div>
   );
};

export default RightArrowBlock;