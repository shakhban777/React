import React from "react";
import arrowLeft from "../../../assets/img/icons/arrow-left.svg";
import '../forecast.scss';

type LeftArrowTypeProps = {
  scrollToThePrevForecast?: () => void,
}

const LeftArrowBlock: React.FC<LeftArrowTypeProps> = ({
                                                        scrollToThePrevForecast
                                                      }) => {
   return (
      <div onClick={scrollToThePrevForecast}
           className="weather-blocks__arrow-left">
         <img src={arrowLeft} alt="arrow-left"/>
      </div>
   );
};

export default LeftArrowBlock;