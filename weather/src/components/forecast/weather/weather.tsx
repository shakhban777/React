import React from "react";
import {WeatherInfoType} from "../../app/app";
import LeftArrowBlock from "../left-arrow/left-arrow";
import RightArrowBlock from "../right-arrow/right-arrow";
import WeatherCard from "../../weather-cards/weather-card";
import '../forecast.scss';

type WeatherType = {
   sevenDaysWeatherData?: WeatherInfoType[],
   showAllWeatherCardsForSevenDays?: boolean,
   onPreviousDay?: () => void,
   onNextDay?: () => void,
   blockSelect: 0 | 1
}

const Weather: React.FC<WeatherType> = ({
                                           sevenDaysWeatherData,
                                           showAllWeatherCardsForSevenDays,
                                           onPreviousDay,
                                           onNextDay,
                                           blockSelect
                                        }) => {

   const leftArrow = showAllWeatherCardsForSevenDays ? null : <LeftArrowBlock scrollToThePrevForecast={onPreviousDay}/>;
   const rightArrow = showAllWeatherCardsForSevenDays ? null : <RightArrowBlock scrollToTheNextForecast={onNextDay}/>;

   return (
      <div className='weather-blocks'>
         {leftArrow}
         {
            sevenDaysWeatherData!.map((obj: WeatherInfoType) => {
               return <WeatherCard key={obj.id}
                                   date={obj.date}
                                   icon={obj.icon}
                                   temperature={obj.temperature}
                                   blockSelect={blockSelect}/>
            })
         }
         {rightArrow}
      </div>
   );
};

export default Weather;