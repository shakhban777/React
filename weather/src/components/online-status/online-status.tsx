import React, {useState, useEffect} from "react";
import {ReactComponent as OfflineIcon} from './offline.svg';
import './online-status.scss';

const OnlineStatus: React.FC = () => {
	const [isOnline, setIsOnline] = useState(true);

    function handleNetworkChange(): void {
        if (navigator.onLine) {
            setIsOnline(true);
        } else {
            setIsOnline(false);
        }
    }

     useEffect(() => {
        handleNetworkChange();
        window.addEventListener('online', handleNetworkChange);
        window.addEventListener('offline', handleNetworkChange);
    }, [])


	return (
		<div className='offline'>
			 {!isOnline && <OfflineIcon/>}
		</div>
	);
};

export default OnlineStatus;