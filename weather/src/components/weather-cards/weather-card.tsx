import React from "react";
import './seven-days-weather-card.scss';
import './historic-weather-card.scss';

type WeatherTypeProps = {
  date: string,
  icon: string,
  temperature: string,
}

type WhichCardRenderTypeProp = {
  blockSelect: number
}

const WeatherCard: React.FC<WeatherTypeProps & WhichCardRenderTypeProp> = ({date, icon, temperature, blockSelect}) => {
  if (blockSelect === 0) {
    return <WeeklyCard date={date} icon={icon} temperature={temperature}/>
  } else {
    return <HistoricCard date={date} icon={icon} temperature={temperature}/>
  }
};

const WeeklyCard: React.FC<WeatherTypeProps> = ({date, icon, temperature}) => {
  return (
    <div className='weather-card'>
      <div className="weather-card__flex">
        <div className="weather-card__date">{date}</div>
        <img className='weather-card__image' src={icon} alt=""/>
        <div className="weather-card__temp">{temperature}</div>
      </div>
    </div>
  );
}

const HistoricCard: React.FC<WeatherTypeProps> = ({date, icon, temperature}) => {
  const image = icon ? <img className='historic-weather-card__image' src={icon} alt=""/> : null;
  const text = date
    ? <div className="historic-weather-card__date">{date}</div>
    : <div className="error-text">Please enter last 5 days or choose from calendar</div>

  return (
    <div className='historic-weather-card'>
      <div className="historic-weather-card__flex">
        {text}
        {image}
        <div className="historic-weather-card__temp">{temperature}</div>
      </div>
    </div>
  );
}

export default WeatherCard;