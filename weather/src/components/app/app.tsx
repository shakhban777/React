import React, {useEffect, useState} from 'react';
import WeatherApiService from "../../api/api";
import Title from '../title/title';
import Forecast from "../forecast/forecast";
import OnlineStatus from "../online-status/online-status";
import './app.scss';

export type CityType = {
   name: string,
   lat: number,
   lon: number
}

export type WeatherInfoType = {
   id?: number,
   date: string,
   icon: string,
   temperature: string
}

type LocationType = {
   lat: number | null,
   lon: number | null,
}

function useWindowDimensions() {
   const [width, setWidth] = React.useState<number>(window.innerWidth);
   useEffect(() => {
      window.addEventListener("resize", updateWidth);
      return () => window.removeEventListener("resize", updateWidth);
   });

   const updateWidth = () => {
      setWidth(window.innerWidth);
   };
   return width;
}

const App: React.FC = () => {
   const parsedURL = new URL(window.location.toString());
   const serchingParams = parsedURL.search;
   const windowWidth = useWindowDimensions();
   const citiesArray = [
      {name: 'Самара', lat: 53.195873, lon: 50.100193},
      {name: 'Тольятти', lat: 53.507836, lon: 49.420393},
      {name: 'Саратов', lat: 51.533557, lon: 46.034257},
      {name: 'Казань', lat: 55.796127, lon: 49.106405},
      {name: 'Краснодар', lat: 45.035470, lon: 38.975313},
      {name: 'Моё местоположение', lat: 0, lon: 0}
   ];

   const [cities] = useState<CityType[]>(citiesArray);
   const [location, setLocation] = useState<LocationType[]>([{lat: null, lon: null}, {lat: null, lon: null}]);
   const [date, setDate] = useState<number | null>(null);
   const [sevenDaysWeatherData, setSevenDaysWeatherData] = useState<WeatherInfoType[]>([]);
   const [historicWeatherData, setHistoricWeatherData] = useState<WeatherInfoType>({date: '', icon: '', temperature: ''});
   const [showSevenDaysForecast, setShowSevenDaysForecast] = useState<boolean>(false);
   const [showHistoricForecast, setShowHistoricForecast] = useState<boolean>(false);
   const [showAllWeatherCardsForSevenDays, setShowAllWeatherCardsForSevenDays] = useState<boolean>(false);
   const [weatherDays, setWeatherDays] = useState<number>(0);

   const [locationForSevenDaysWeather, locationForHistoricWeather] = location;

   useEffect(() => {
      const lat = locationForSevenDaysWeather.lat;
      const lon = locationForSevenDaysWeather.lon;

      if (lat && lon) {
         const weatherService = new WeatherApiService();

         weatherService.getWeatherForSevenDays(lat, lon)
            .then(response => {
               setSevenDaysWeatherData([]);
               if (showAllWeatherCardsForSevenDays) {
                  setSevenDaysWeatherData(response);
               } else {
                  setSevenDaysWeatherData(response.slice(weatherDays, 3 + weatherDays));
               }
               setShowSevenDaysForecast(true);
            })
      }
   }, [locationForSevenDaysWeather, weatherDays, showAllWeatherCardsForSevenDays])

   useEffect(() => {
      const lat = locationForHistoricWeather.lat;
      const lon = locationForHistoricWeather.lon;

      if (lat && lon && date) {
         const weatherService = new WeatherApiService();
         weatherService.getWeatherForHistoricDate(lat, lon, date)
            .then(historicForecast => {
               setHistoricWeatherData(historicForecast);
               setShowHistoricForecast(true);
            })
      }
   }, [locationForHistoricWeather, date])

   useEffect(() => {
      if (windowWidth <= 660) {
         setShowAllWeatherCardsForSevenDays(true);
      } else {
         setShowAllWeatherCardsForSevenDays(false);
      }
   }, [windowWidth])

   const changeLocationHandler = (coords: string, blockSelect: number) => {

      function setNewLocation(latitude: string, longitude: string) {
         const newLocation = {
            lat: +latitude,
            lon: +longitude
         };

         setLocation(prevState => prevState.map(el => {
            if (el === prevState[blockSelect]) {
               return newLocation;
            }
            return el;
         }));
      }

      const [latitude, longitude] = coords.split(', ');

      if (+latitude === 0 && +longitude === 0) {
         if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
               setNewLocation(position.coords.latitude.toString(), position.coords.longitude.toString());
            });
         } else {
            console.log('Geolocation is not supported on your device...');
         }
      } else {
         setNewLocation(latitude, longitude);
      }
   };

    useEffect(() => {
      if (serchingParams) {
         const coordsArray = serchingParams.match(/-?\d+\.\d+/g);
         if (coordsArray) {
             const coords = `${coordsArray[0]}, ${coordsArray[1]}`;
             changeLocationHandler(coords, 0);
             changeLocationHandler(coords, 1);
         }
      }
   }, [serchingParams])

   const nextDayHandler = () => {
      if (0 <= weatherDays && weatherDays < 5) {
         setWeatherDays(prevState => ++prevState);
      }
   };

   const previousDayHandler = () => {
      if (0 < weatherDays && weatherDays <= 5) {
         setWeatherDays(prevState => --prevState);
      }
   };

   return (
      <div className='app'>
         <div className='_container'>
            <header className='app__title'>
               <Title/>
					<OnlineStatus/>
            </header>
            <main className='app__blocks'>
               <Forecast cities={cities}
                         title={"7 Days Forecast"}
                         showSevenDaysForecast={showSevenDaysForecast}
                         onChangeLocation={changeLocationHandler}
                         onPreviousDay={previousDayHandler}
                         onNextDay={nextDayHandler}
                         showAllWeatherCardsForSevenDays={showAllWeatherCardsForSevenDays}
                         sevenDaysWeatherData={sevenDaysWeatherData}
                         blockSelect={0}/>

               <Forecast cities={cities}
                         title={'Forecast for a Date in the Past'}
                         showHistoricForecast={showHistoricForecast}
                         onChangeLocation={changeLocationHandler}
                         onChangeDate={(date: number) => setDate(date)}
                         historicWeatherData={historicWeatherData}
                         blockSelect={1}/>
            </main>
         </div>
      </div>
   );
};

export default App;