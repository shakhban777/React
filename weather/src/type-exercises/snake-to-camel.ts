type SnakeToCamel<T extends string> = T extends `${infer BEFORE}_${infer AFTER}`
  ? `${Lowercase<BEFORE>}${Capitalize<SnakeToCamel<AFTER>>}`
  : `${Lowercase<T>}`;

export {}