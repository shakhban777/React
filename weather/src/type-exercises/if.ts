type If<T extends Boolean, V, Z> = T extends true ? V : Z;

export {}