type Trim<Base extends string> = Base extends ` ${infer String}` | `${infer String} `
  ? Trim<String>
  : Base;

export {}