type CustomOmit<T, K> = {
  [PT in keyof T as Exclude<PT, K>]: T[PT]
};

export {}