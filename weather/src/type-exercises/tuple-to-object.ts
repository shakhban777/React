type TupleToObject<T extends readonly string[] | readonly number[], V> = {
  [Key in T[number]] : V;
};

export {}