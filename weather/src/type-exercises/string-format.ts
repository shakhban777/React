type StringFormat<T extends string> = T extends `${infer BEFORE}%${infer AFTER}`
  ? AFTER extends `s${infer SECOND_AFTER}`
    ? (s: string) => StringFormat<`${SECOND_AFTER}`>
    : AFTER extends `d${infer SECOND_AFTER}`
      ? (d: number) => StringFormat<`${SECOND_AFTER}`>
      : StringFormat<`${BEFORE}${AFTER}`>
  : string;

export {}