type StringReplace<A extends string, B extends string, C extends string> = A extends `${infer BEFORE}${B}${infer AFTER}`
  ? StringReplace<`${BEFORE}${C}${AFTER}`, B, C>
  : A extends `${infer BEFORE}${Uppercase<B>}${infer AFTER}`
    ? StringReplace<`${BEFORE}${Uppercase<C>}${AFTER}`, B, C>
    : A extends `${infer BEFORE}${Capitalize<B>}${infer AFTER}`
      ? StringReplace<`${BEFORE}${Capitalize<C>}${AFTER}`, B, C>
      : A;

export {}