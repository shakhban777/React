type PaddingType = {
  top: `${number}px`,
  right: `${number}px`,
  bottom: `${number}px`,
  left: `${number}px`
};

type PaddingZeroType = {
  top: 0,
  right: 0,
  bottom: 0,
  left: 0
}

function padding(): PaddingZeroType;
function padding(padding: number): PaddingType;
function padding(paddingVertical: number, paddingHorizontal: number): PaddingType;
function padding(paddingTop: number, paddingHorizontal: number, paddingBottom: number): PaddingType;
function padding(paddingTop: number, paddingRight: number, paddingBottom: number, paddingLeft: number): PaddingType;

function padding(firstPadding?: number, secondPadding?: number, thirdPadding?: number, fourthPadding?: number): PaddingType | PaddingZeroType {
  if (firstPadding && secondPadding && thirdPadding && fourthPadding) {
    return {
      top: `${firstPadding}px`,
      right: `${secondPadding}px`,
      bottom: `${thirdPadding}px`,
      left: `${fourthPadding}px`
    };
  } else if (firstPadding && secondPadding && thirdPadding) {
    return {
      top: `${firstPadding}px`,
      right: `${secondPadding}px`,
      bottom: `${thirdPadding}px`,
      left: `${secondPadding}px`
    };
  } else if (firstPadding && secondPadding) {
    return {
      top: `${firstPadding}px`,
      right: `${secondPadding}px`,
      bottom: `${firstPadding}px`,
      left: `${secondPadding}px`
    };
  } else if (firstPadding) {
    return {
      top: `${firstPadding}px`,
      right: `${firstPadding}px`,
      bottom: `${firstPadding}px`,
      left: `${firstPadding}px`
    };
  } else {
    return {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    };
  }
}

export {}