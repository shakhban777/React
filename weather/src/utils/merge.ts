const merge = <T, V>(obj1: T, obj2: V): T & V => {
  return { ...obj1, ...obj2 };
}

export {}